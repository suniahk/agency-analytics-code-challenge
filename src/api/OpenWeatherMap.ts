import OpenWeatherMapApiResponse from '../types/OpenWeatherMapApiResponse'
import Screen from '../types/Screen'
import WorldLocation from '../types/WorldLocation'

const createUrlFromLocation = ( location: WorldLocation ): string => {
  // apiUrl and exculsions are seperate simply to keep line length <80 characters.
  const apiUrl = `https://api.openweathermap.org/data/2.5/onecall`
  const exclusions = 'minutely,hourly,alerts'

  return `${apiUrl}?lat=${location.latitude}&lon=${location.longitude}&exclude=${exclusions}&units=metric&appid=${process.env.REACT_APP_API_KEY}`
}

export const getWeatherDataAtLocation = async ( location: WorldLocation ): Promise<OpenWeatherMapApiResponse> => {
  const apiResponse = await fetch( createUrlFromLocation( location ) )
  return apiResponse.json()
}

export const loadScreenData = async( location: WorldLocation ): Promise<Screen> => {
  const apiData = await getWeatherDataAtLocation( location )

  return {
    location: location,
    data: apiData
  }
}