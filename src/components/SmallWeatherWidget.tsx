import React from "react"
import styles from "../styles/WeatherWidget.less"
import WeatherState from "../types/WeatherState"
import WeatherIcon from "./WeatherIcon"

type SmallWeatherWidgetProps = {
  when: string,
  temp: number,
  weather: WeatherState
}

type SmallWeatherWidgetState = {
  date: Date
}

export default class SmallWeatherWidget extends React.Component<SmallWeatherWidgetProps, SmallWeatherWidgetState> {
  constructor( props: SmallWeatherWidgetProps ) {
    super( props )

    // API Gives us a standard UNIX timestamp, which is in seconds.
    // Javascript uses milliseconds so we need to update that
    this.state = {
      date: new Date( parseInt(this.props.when) * 1000 )
    }
  }
  render() {
    const dateLabel = this.state.date.toLocaleDateString( "en-us", { weekday: 'short' } )
    
    return (
      <div className={ `${styles.widgetContainer} ${styles.smallWidget}` }>
        <h1>{ dateLabel }</h1>
        <div>
          <WeatherIcon which={ this.props.weather.icon } />
          <h2>{ this.props.temp }&deg;</h2>
        </div>
      </div>
    )
  }
}