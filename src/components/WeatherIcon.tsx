type WeatherIconProps = {
  which: string
  size ?: number
}

const WeatherIcon = (props:WeatherIconProps): JSX.Element => ( 
  <img src={ `http://openweathermap.org/img/wn/${props.which}@${props.size?props.size:2}x.png` } alt={`${ props.which } Icon` } />
)

export default WeatherIcon