import React from "react"
import styles from "../styles/WeatherWidget.less"
import WeatherState from "../types/WeatherState"
import WeatherIcon from "./WeatherIcon"

type WeatherWidgetProps = {
  when: string,
  temp: number,
  weather: WeatherState
}

export default class WeatherWidget extends React.Component<WeatherWidgetProps> {
  render() {
    return (
      <div className={ styles.widgetContainer }>
        <h1>{ this.props.when }</h1>
        <div>
          <WeatherIcon which={ this.props.weather.icon } size={4} />
          <div>
            <h2>{ this.props.temp }&deg;</h2>
            <p>{ this.props.weather.main }</p>
          </div>
        </div>
      </div>
    )
  }
}