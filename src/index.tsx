import React from 'react';
import * as ReactDOMClient from 'react-dom/client';
import WeatherScreen from './screens/WeatherScreen';
import styles from './styles/index.less';

const container = document.getElementById('root')

if( container ) {
  const root = ReactDOMClient.createRoot(
    container
  );

  root.render(
    <React.StrictMode>
      <div className={ styles.wrapper }>
        <WeatherScreen />
      </div>
    </React.StrictMode>
  )
}