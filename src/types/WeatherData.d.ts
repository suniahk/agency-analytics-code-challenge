import WeatherState from "./WeatherState"

type WeatherData = {
  temp: number & { max: number }
  weather: WeatherState[]
  dt : string
}

export default WeatherData