type WeatherState = {
  id: number
  icon: string
  main: string
}

export default WeatherState