import OpenWeatherMapApiResponse from "./OpenWeatherMapApiResponse"
import WorldLocation from "./WorldLocation"

type Screen = {
  location: WorldLocation
  data: OpenWeatherMapApiResponse
}

export default Screen