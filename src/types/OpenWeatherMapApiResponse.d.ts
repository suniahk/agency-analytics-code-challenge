import WeatherData from "./WeatherData"

// Normally you'd want to use the API docs to properly implement the type.
// For this example code, we'll just implement the top-level objects that we need.
type OpenWeatherMapApiResponse = {
  current: WeatherData,
  daily: WeatherData[]
}

export default OpenWeatherMapApiResponse