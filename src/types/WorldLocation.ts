type WorldLocation = {
  name: string
  latitude: number
  longitude: number
}

export default WorldLocation