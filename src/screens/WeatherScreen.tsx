import React from "react"
import { loadScreenData } from "../api/OpenWeatherMap"
import SmallWeatherWidget from "../components/SmallWeatherWidget"
import WeatherWidget from "../components/WeatherWidget"
import styles from '../styles/WeatherScreen.less'
import Screen from "../types/Screen"
import WorldLocation from "../types/WorldLocation"

type WeatherScreenProps = {

}

type WeatherScreenState = {
  currentScreenIndex: number
  screenData: Screen[]
}

export default class WeatherScreen extends React.Component<WeatherScreenProps, WeatherScreenState> {

  constructor( props: WeatherScreenProps ) {
    super( props )

    this.state = {
      currentScreenIndex: 0,
      screenData: []
    }
  }

  componentDidMount() {
    const locations: WorldLocation[] = [
      {
        name: "Windsor",
        longitude: -83.03,
        latitude: 42.31
      },
      {
        name: "London",
        longitude: -0.12,
        latitude: 51.50
      },
      {
        name: "Sidney",
        longitude: 151.20,
        latitude: -33.86
      }
    ]

    locations.forEach( location => loadScreenData( location ).then( apiData => {
      let updatedData = this.state.screenData

      const newData = apiData;

      // daily[0] is today still, so skip that entry and get the next 4.
      newData.data.daily = newData.data.daily.slice( 1,5 )

      locations.forEach( ( ( locationData, index ) => {
        if( locationData.name === apiData.location.name ) {
          updatedData[ index ] = newData
        }
      } ) )

      this.setState( {
        screenData: updatedData
      } )
    } ) )

    
  }

  handleScreenChange( newScreen: number ) {
    this.setState( {
      currentScreenIndex: newScreen
    } )
  }

  render() {
    const currentScreen = this.state.screenData[this.state.currentScreenIndex]

    return (
      <div className={ styles.screen }>
        <div className={ styles.buttons }>
          { this.state.screenData.map( 
            ( screen, index ) => <button className={ index === this.state.currentScreenIndex ? styles.current : '' } key={ `${screen.location.name}${index}` } onClick={ () => this.handleScreenChange( index ) }>{ screen.location.name }</button> ) }
        </div>
        { !!currentScreen &&
          <div className={ styles.dataList }>
            <WeatherWidget when="Today" temp={ Math.round(currentScreen.data.current.temp) } weather={ currentScreen.data.current.weather[0] } />
            <div className={ styles.futureData }>
              { currentScreen.data.daily.map( weatherData => (
                <SmallWeatherWidget 
                  when={ weatherData.dt }
                  temp={  Math.round( weatherData.temp.max ) }
                  weather={ weatherData.weather[0] }
                  key={ `weatherOn${ weatherData.dt }` } /> 
              ) ) }
            </div>
          </div>
        }
        { !currentScreen && <p>Please Wait, Loading...</p> }
      </div>
    )
  }
}