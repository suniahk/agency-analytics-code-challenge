# Agency Analytics Code Challenge
### Code By Alex Watson, Apr. 7, 2022
- Coded to spec, including "bonus" categories
- Created with react-create-app for simplicity, ejected for less implementation.
- I Tried to match the fonts/colours as best as I could to the mockup provided.
- App is "relatively" mobile friendly; I didn't add in any media queries to fully optimize it but flexbox does a decent job otherwise. 
- Uses OpenWeatherMap OneCall API
- API has a limit of 1000 calls/day; since the load for the app is so low (and won't get any larger), I didn't implement any client-side rate limits.
- No caching either as this was geared towards relative simplicity (although in a production app it really should cache the api responses, at least once a day)